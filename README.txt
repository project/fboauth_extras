
FBOauth Extras
------------------
The main thing that FBOauth Extras does is allow for the friends list to be downloaded
from facebook for a user and link to anyone who has an account on your Drupal site

User Relationships is a pretty self-descriptive module. It allows users the
opportunity to create relationships between each other.

The module has been broken up into component pieces in an effort to keep the size
down and the speed up. The list of modules and a quick intro about each is below

Send comments to Jeff Smick: http://drupal.org/user/107579/contact, or post an issue at
http://drupal.org/project/user_relationships.

Requirements
------------
Drupal 7
FBOauth Module
Either User Relationships or Relation module


Installation
------------
1.  Copy the fboauth_extras folder to the appropriate Drupal (sites/all/modules or sites/default/modules) directory.

2.  Download and Enable User Relationships or Relations modules in the "Modules" administration screen.

3.  Enable FBOauth Extras in the "Modules" administration screen.


to be continued...



The import will start when the fboauth_extras_action_execute_friend_import()
function fires, manually, or by visiting the /fboauth/friend-import URL.

You may want to set up an action to fire it with the Rules module.

Note, Facebook prohibits the importing of friends who are NOT ALREADY using
the app.
